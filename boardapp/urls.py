from django.urls import path
from .views import signupfunc, loginfunc, listfunc, logoutfunc, detailfunc, goodfunc, readfunc, BoardCreate, popularfunc, my_pagefunc, popularfunc2, couponfunc, likedfunc
from boardapp import views

urlpatterns = [
    path('signup/', signupfunc, name='signup'),
    path('', loginfunc, name='login'),
    path('list/', listfunc, name='list'),
    path('logout/', logoutfunc, name='logout'),
    path('detail/<int:pk>', detailfunc, name="detail"),
    path('good/<int:pk>', goodfunc, name='good'),
    path('read/<int:pk>', readfunc, name='read'),
    path('create/', BoardCreate.as_view(), name='create'),
    path("delete/<int:pk>",views.delete,name="delete"),
    path('popular/', popularfunc, name='popular'),
    path('popular2/', popularfunc2, name='popular2'),
    path('my_page/', my_pagefunc, name='my_page'),
    path('coupon/', couponfunc, name = 'coupon'),
    path('liked/', likedfunc, name='liked'),
]
