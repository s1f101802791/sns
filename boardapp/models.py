from django.db import models
from django.utils import timezone

# Create your models here.

class BoardModel(models.Model):
  title = models.CharField(max_length=100)
  content = models.TextField(max_length=500)
  author = models.CharField(max_length=100, null=True)
  liked = models.CharField(max_length=100, null=True)
  date = models.DateTimeField('作成日', default=timezone.now)
  images = models.ImageField(upload_to='')
  good = models.IntegerField(null=True, blank=True, default=0)
  read = models.IntegerField(null=True, blank=True, default=0)
  readtext = models.CharField(max_length=200, null=True, blank=True, default='a')
  

